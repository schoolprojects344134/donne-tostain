import { createApp } from 'vue'
import HomeApp from './HomeApp'

//import './assets/main.css'
import 'bootstrap'

createApp(HomeApp).mount('#app')
