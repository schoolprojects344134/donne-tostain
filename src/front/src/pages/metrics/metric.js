import { createApp } from 'vue'
import App from './MetricApp'

//import './assets/main.css'
import 'bootstrap'

createApp(App).mount('#app')
