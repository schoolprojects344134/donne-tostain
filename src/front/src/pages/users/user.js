import { createApp } from 'vue'
import UserApp from './UserApp'

//import './assets/main.css'
import 'bootstrap'

createApp(UserApp).mount('#app')
