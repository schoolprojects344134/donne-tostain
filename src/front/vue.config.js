module.exports = {
    pages: {
        'home': {
            entry: './src/pages/home/home.js',
            template: 'public/index.html',
            title: 'Home',
            chunks: [ 'home' ]
        },
        'metrics': {
            entry: './src/pages/metrics/metric.js',
            template: 'public/index.html',
            title: 'Metrics',
            chunks: [ 'metrics' ]
        },
        'users': {
            entry: './src/pages/users/user.js',
            template: 'public/index.html',
            title: 'Users',
            chunks: [ 'users' ]
        }
    }

}