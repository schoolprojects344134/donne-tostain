package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.entity.*;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.blame.BlameResult;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.TreeWalk;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.util.*;

public class JGit {
    private final fr.uge.cloutmetrics.api.entity.Repository repository;
    private final MetricGeneration metrics;

    public JGit(fr.uge.cloutmetrics.api.entity.Repository repository) {
        Objects.requireNonNull(repository);
        this.repository = repository;
        metrics = new MetricGeneration(repository.repositoryId());
    }

    public fr.uge.cloutmetrics.api.entity.Repository cloneRepo() throws IOException, GitAPIException {
        Objects.requireNonNull(repository);

        var dirPath = Path.of("res/directories");
        var url = repository.url();
        var repoName = url.substring(url.lastIndexOf("/") + 1);
        if(!Files.exists(dirPath)) {
            Files.createDirectories(dirPath);
        }
        dirPath = Path.of("res/directories/"+repoName);
        System.out.println("the path is "+dirPath);

        // then clone
        if(!Files.exists(dirPath)) {
            System.out.println("Cloning from " + url + " to " + dirPath);
            try (var result = Git.cloneRepository()
                    .setURI(url)
                    .setDirectory(dirPath.toFile())
                    .setCredentialsProvider(new UsernamePasswordCredentialsProvider(repository.tokenName(), repository.token()))
                    .call()) {
                // Note: the call() returns an opened repository already which needs to be closed to avoid file handle leaks!
            }
        }

        var date = new Date(System.currentTimeMillis()).toString();
        return new fr.uge.cloutmetrics.api.entity.Repository(-1, url, repoName, date, date, repository.tokenName(), repository.token(), "none");
    }

    public MetricGeneration generateAllData(long id) throws IOException, GitAPIException {
        Objects.requireNonNull(repository);
        var url = repository.url();
        var repoName = url.substring(url.lastIndexOf("/") + 1);
        var dirPath = Path.of("res/directories/"+repoName);

        if(!Files.exists(dirPath)) {
            cloneRepo();
        }

        try (var git = Git.open(dirPath.toFile())) {
            try (var repository = git.getRepository()) {
                System.out.println("Having repository: " + repository.getDirectory());
                var head = repository.exactRef("HEAD");
                if(head == null) {
                    throw new IllegalStateException("No HEAD detected for "+repository);
                }

                // a RevWalk allows to walk over commits based on some filtering that is defined
                try (RevWalk walk = new RevWalk(repository)) {
                    RevCommit commit = walk.parseCommit(head.getObjectId());
                    RevTree tree = commit.getTree();

                    // now use a TreeWalk to iterate over all files in the Tree recursively
                    // you can set Filters to narrow down the results if needed
                    try (TreeWalk treeWalk = new TreeWalk(repository)) {
                        treeWalk.addTree(tree);
                        treeWalk.setRecursive(true);
                        while (treeWalk.next()) {
                            var path = treeWalk.getPathString();
                            var fileName = treeWalk.getNameString();
                            var objectId = treeWalk.getObjectId(0);
                            var loader = repository.open(objectId);

                            try (var fileStream = loader.openStream()) {
                                if(fileName.equals("LICENSE")) {
                                    metrics.setLicence(getLicense(fileStream));
                                    continue;
                                }
                                blame(repository, path, fileName);
                            }
                        }
                    } catch (GitAPIException e) {
                        throw new RuntimeException(e);
                    }
                    walk.dispose();
                }
            }
        }
        return metrics;
    }

    private String getLicense(InputStream stream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        return reader.readLine().trim();
    }

    private Language computeLanguage(String fileName) {
        if(!fileName.matches(".+\\..*")) {
            return Language.OTHER;
        }
        var extension = fileName.substring(fileName.lastIndexOf(".") + 1);
        var language = Language.getFromExtension(extension);
        metrics.addLanguage(language);
        return language;
    }

    private void blame(Repository repository, String filePath, String fileName) throws GitAPIException, IOException {
        var language = computeLanguage(fileName);

        var result = new Git(repository).blame().setFilePath(filePath)
                .setTextComparator(RawTextComparator.WS_IGNORE_ALL).call();

        if(language.equals(Language.JAVA)) {
            computeJavaFile(result);
        } else if(language.equals(Language.MARKDOWN)) {
            countLinePerUser(result, Optional.of(Badge.DOCTOR));
        } else if (!language.equals(Language.OTHER)){
            countLinePerUser(result, Optional.empty());
        } else {
            if(fileName.equals("Makefile") || fileName.equals("makefile") || fileName.equals("pom.xml")) {
                countLinePerUser(result, Optional.of(Badge.BUILDER));
            } else if (fileName.equals(".travis.yml") || fileName.equals("config.yml") || fileName.equals("Dockerfile") || fileName.equals("docker-compose.yml") || filePath.matches(".*\\.github/workflows/.*\\.yml")) {
                countLinePerUser(result, Optional.of(Badge.DEVOPS));
            } else if (fileName.equals("module-info.java ")) {
                countLinePerUser(result, Optional.of(Badge.ARCHITECT));
            } else {} // Decided to not add contribution to an author of an extension not known (to prevent counting lines on a pdf or jpeg)
        }
    }

    private void countLinePerUser(BlameResult result, Optional<Badge> optionalBadge) {
        var rawText = result.getResultContents();
        for (int i = 0; i < rawText.size(); i++) {
            var sourceAuthor = result.getSourceAuthor(i);
            optionalBadge.ifPresentOrElse((badge) -> {
                metrics.addAuthorBadge(sourceAuthor, badge);
                if(badge.equals(Badge.DOCTOR))  {
                    metrics.addContribution(sourceAuthor);
                }
            }, () -> metrics.addContribution(sourceAuthor));
        }
    }

    private void computeJavaFile(BlameResult blameResult) throws IOException {
        enum State {
            READING_TEXT,
            READING_JAVA_DOC,
            READING_OTHER_DOC
        };

        var state = State.READING_TEXT;
        var rawText = blameResult.getResultContents();
        String line;

        for (int i = 0; i < rawText.size(); i++) {
            line = rawText.getString(i);
            var sourceAuthor = blameResult.getSourceAuthor(i);
            if (state == State.READING_TEXT) {
                if (line.matches(".*\\/\\*\\*.*")) {
                    state = State.READING_JAVA_DOC;
                } else if(line.matches(".*\\/\\*.*")) {
                    state = State.READING_OTHER_DOC;
                } else if (!line.trim().matches("\\/\\/.*")) { // If text does not start with //, it is normal code text
                    metrics.addContribution(sourceAuthor);
                }
            }
            if (!(state == State.READING_TEXT)) {
                if(state == State.READING_JAVA_DOC) {
                    metrics.addJavaDoc(sourceAuthor);
                } else {
                    metrics.addAuthorBadge(sourceAuthor, Badge.DOCTOR);
                    metrics.addContribution(sourceAuthor);
                }
                if (line.matches(".*\\*\\/.*")) {
                    state = State.READING_TEXT;
                }
            }
        }
    }
}