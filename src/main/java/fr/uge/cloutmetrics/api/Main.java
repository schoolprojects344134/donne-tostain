package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.server.Server;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws GitAPIException {
      var server = new Server();
      server.startWebServer();
      System.out.println("To stop webserver you should use the command : exit");
      try(var scanner = new Scanner(System.in)){
        while(scanner.hasNext()){
          if(scanner.next().equals("exit")){
            server.shutdownWebServer();
            break;
          }
        }
      }
      System.exit(0);
    }
}
