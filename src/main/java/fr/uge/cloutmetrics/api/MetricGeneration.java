package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.entity.*;
import io.helidon.dbclient.DbClient;
import org.eclipse.jgit.lib.PersonIdent;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class MetricGeneration {
  private final long repositoryId;
  private final HashMap<User, Integer> contribution = new HashMap<>();
  private final HashMap<User, Integer> java_documentation = new HashMap<>();
  private final HashMap<String, Integer> languages = new HashMap<>();
  private final HashMap<Badge, Map<User, Integer>> mapOfBadges;
  private final ArrayList<BadgeMetric> badges = new ArrayList<>();
  private String license = "none";

  public MetricGeneration(long repositoryId){
    this.repositoryId = repositoryId;
    var allBadges = Badge.values();
    mapOfBadges = new HashMap<>(allBadges.length);
    for (Badge badge : allBadges) {
      mapOfBadges.put(badge, new HashMap<>());
    }
  }

  public void addInDataBase(DbClient db, Repository repository) throws ExecutionException, InterruptedException {
    Objects.requireNonNull(db);

    Repository.updateRepository(db, new Repository(repositoryId, repository.url(), repository.name(), repository.creation_date(), repository.refresh_date(), repository.tokenName(), repository.token(), license));

    var ratioLanguage = getRatioLanguage();
    for(var entry: ratioLanguage.entrySet()) {
      LanguageMetric.addLanguage(db, new LanguageMetric(repositoryId, entry.getKey(), entry.getValue()));
    }

    var usersId = new HashMap<String, Long>();

    for(var contributionEntry: contribution.entrySet()) {
      var user = contributionEntry.getKey();
      var nbLines = contributionEntry.getValue();

      var generatedUser = User.addUser(db, new User(-1, user.pseudo(), user.email()));
      usersId.put(generatedUser.email(), generatedUser.userId());
      Author.addAuthor(db, new Author(repositoryId, generatedUser.userId(), nbLines));
    }

    for(var docEntry: java_documentation.entrySet()) {
      var userDocumentation = docEntry.getKey();
      var lines = docEntry.getValue();
      var userId = usersId.get(userDocumentation.email());
      if(userId == null) {
        userId = User.addUser(db, new User(-1, userDocumentation.pseudo(), userDocumentation.email())).userId();
      }

      var documentation = new DocumentationMetric(repositoryId, userId, lines);
      DocumentationMetric.addDocumentation(db,documentation);
    }

    var badges = getBadges(usersId);

    for(var badge: badges) {
      BadgeMetric.addBadge(db, badge);
    }
  }

  private ArrayList<BadgeMetric> getBadges(Map<String, Long> usersId) {
    mapOfBadges.forEach((badge, map) -> {
      addBadge(usersId, map, badge.name());
    });
    return badges;
  }

  private void addBadge(Map<String, Long> usersId, Map<User, Integer> mapOfBadge, String nameOfBadge) {
    var optionalDoctor = mapOfBadge.entrySet().stream().max(Comparator.comparingInt(Map.Entry::getValue));
    if(optionalDoctor.isPresent()) {
      var doctor = optionalDoctor.get();
      var userId = usersId.get(doctor.getKey().email());
      if(userId == null) {
        throw new IllegalStateException("The doctor username ("+doctor.getKey().email()+") was not found in the list given");
      }
      badges.add(new BadgeMetric(repositoryId, userId, nameOfBadge));
    }
  }

  public void setLicence(String licence) {
    Objects.requireNonNull(licence);
    this.license = licence;
  }

  private HashMap<String, Integer> getRatioLanguage() {
    var sum = Double.valueOf(languages.values().stream().mapToDouble(Double::valueOf).sum());
    final var newMap = new HashMap<String, Integer>();
    languages.forEach((key, value) -> {
      var newValue = (int) Math.round(value/sum*100);
      newMap.put(key, newValue);
    });
    return newMap;
  }

  public void addLanguage(Language language) {
    Objects.requireNonNull(language);
    languages.merge(language.name(), 1, (oldValue, newValue) -> oldValue + 1);
  }

  public void addContribution(PersonIdent sourceAuthor) {
    Objects.requireNonNull(sourceAuthor);
    contribution.merge(new User(-1, sourceAuthor.getName().toLowerCase(Locale.ROOT), sourceAuthor.getEmailAddress()), 1, (oldValue, newValue) -> oldValue+1);
  }

  public void addJavaDoc(PersonIdent sourceAuthor) {
    Objects.requireNonNull(sourceAuthor);
    java_documentation.merge(new User(-1, sourceAuthor.getName().toLowerCase(Locale.ROOT), sourceAuthor.getEmailAddress()), 1, (oldValue, newValue) -> oldValue+1);
  }

  public void addAuthorBadge(PersonIdent sourceAuthor, Badge badge) {
    Objects.requireNonNull(sourceAuthor);
    Objects.requireNonNull(badge);

    var map = mapOfBadges.get(badge);
    if(map == null) {
      throw new IllegalArgumentException();
    }

    map.merge(new User(-1, sourceAuthor.getName().toLowerCase(Locale.ROOT), sourceAuthor.getEmailAddress()), 1, (oldValue, newValue) -> oldValue+1);
  }
}
