package fr.uge.cloutmetrics.api.database;

import io.helidon.dbclient.DbClient;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class DbManager {
    private DbManager() {
        throw new UnsupportedOperationException("Instances of DbManager class are not allowed");
    }

    /**
     * Initialize database by creating tables
     *
     * @param dbClient database to initialize
     */
    public static void init(DbClient dbClient) {
        Objects.requireNonNull(dbClient);
        executeRequest(dbClient,"create-repositories");
        executeRequest(dbClient,"create-authors");
        executeRequest(dbClient,"create-dependencies");
        executeRequest(dbClient,"create-users");
        executeRequest(dbClient,"create-badges");
        executeRequest(dbClient,"create-metrics");
        executeRequest(dbClient,"create-language-metrics");
        executeRequest(dbClient,"create-documentation");
    }

    /**
     * Remove tables from database
     *
     * @param dbClient database client
     */
    public static void clear(DbClient dbClient) {
        Objects.requireNonNull(dbClient);
        executeDelete(dbClient,"delete-repositories");
        executeDelete(dbClient,"delete-users");
        executeDelete(dbClient,"delete-language-metrics");
        executeDelete(dbClient,"delete-authors");
        executeDelete(dbClient,"delete-badges");
        executeDelete(dbClient,"delete-documentation");
    }

    private static void executeRequest(DbClient dbClient, String statement) {
        dbClient.execute(exec -> exec.namedDml(statement)).await();
    }

    private static void executeDelete(DbClient dbClient, String statement) {
        dbClient.execute(exec -> exec.namedDelete(statement)).await();
    }
}