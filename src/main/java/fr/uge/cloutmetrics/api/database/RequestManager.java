package fr.uge.cloutmetrics.api.database;

import fr.uge.cloutmetrics.api.entity.DatabaseStorable;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

public class RequestManager {

  /**
   * Update item into the database using the specified statement
   *
   * @param db database client
   * @param item new item values
   * @param updateStatement SQL statement used to update the element
   * @param <T> type of the element stored into the database
   * @throws ExecutionException
   * @throws InterruptedException
   */
  public static <T> void updateItem(DbClient db, DatabaseStorable<T> item, String updateStatement) throws ExecutionException, InterruptedException {
    Objects.requireNonNull(db);
    Objects.requireNonNull(item);
    Objects.requireNonNull(updateStatement);
    db.execute(exec -> exec.createNamedUpdate(updateStatement).params(item.toNamedParameters()).execute()).get();
  }

  /**
   * Insert item into the database
   *
   * @param db database client
   * @param item item to add
   * @param insertStatement SQL statement used to insert item
   * @param <T> type of the element stored into the database
   * @throws ExecutionException
   * @throws InterruptedException
   */
  public static <T> void insertItem(DbClient db, DatabaseStorable<T> item, String insertStatement) throws ExecutionException, InterruptedException {
    Objects.requireNonNull(db);
    Objects.requireNonNull(item);
    db.execute(exec -> exec.createNamedInsert(insertStatement).params(item.toNamedParameters()).execute()).get();
  }

  /**
   * Insert item into the database and returning the new item inserted.
   *
   * @param db database client
   * @param transform function used to transform a DbRow into the T object
   * @param item item that should be added to the database
   * @param insertStatement SQL statement used to insert item
   * @param selectStatement SQL statement used to select item after insertion
   * @return the new element inserted with the generated id
   * @param <T> type of the element stored into the database
   * @throws ExecutionException
   * @throws InterruptedException
   */
  public static <T> T insertAndGetItem(DbClient db, Function<DbRow,T> transform, DatabaseStorable<T> item, String insertStatement, String selectStatement) throws ExecutionException, InterruptedException {
    Objects.requireNonNull(db);
    Objects.requireNonNull(transform);
    Objects.requireNonNull(item);
    Objects.requireNonNull(insertStatement);
    Objects.requireNonNull(selectStatement);
    //TODO check that no other item can be found with the parameters then throw
    // returning generated id not supported by helidon db client https://github.com/helidon-io/helidon/issues/2279
    db.execute(exec -> exec.createNamedInsert(insertStatement).params(item.toNamedParameters()).execute()).get();
    return db.execute(exec -> exec.createNamedGet(selectStatement).params(item.toNamedParameters()).execute()).get().get().as(transform);
  }

  /**
   * Get all items contained into the database
   *
   * @param db database client
   * @param transform function used to transform a DbRow into the T object
   * @param selectStatement SQL statement used to select items
   * @return the list of all items founded
   * @param <T> type of the element stored into the database
   * @throws ExecutionException
   * @throws InterruptedException
   */
  public static <T> List<T> getAllItems(DbClient db, Function<DbRow,T> transform, String selectStatement) throws ExecutionException, InterruptedException {
    Objects.requireNonNull(db);
    Objects.requireNonNull(transform);
    Objects.requireNonNull(selectStatement);
    return db.execute(exec -> exec.namedQuery(selectStatement).map(row-> row.as(transform))).collectList().get();
  }

  /**
   * Get an item using filters
   *
   * @param db database client
   * @param transform function used to transform a DbRow into the T object
   * @param selectStatement SQL statement used to select the item
   * @param filter objects used to filter the item
   * @return an optional of the item
   * @param <T> type of the element stored into the database
   * @throws ExecutionException
   * @throws InterruptedException
   */
  public static <T> Optional<T> getItemBy(DbClient db, Function<DbRow,T> transform, String selectStatement, Object... filter) throws ExecutionException, InterruptedException {
    Objects.requireNonNull(db);
    Objects.requireNonNull(transform);
    Objects.requireNonNull(selectStatement);
    Objects.requireNonNull(filter);
    return db.execute(exec -> exec.namedGet(selectStatement, filter)).get().map(dbRow -> dbRow.as(transform));
  }

  /**
   * Get a list of items using filters
   *
   * @param db database client
   * @param transform function used to transform a DbRow into the T object
   * @param selectStatement SQL statement used to select items
   * @param filter objects used to filter items
   * @return the list of all items founded with the filters
   * @param <T> type of the element stored into the database
   * @throws ExecutionException
   * @throws InterruptedException
   */
  public static <T> List<T> getItemsWhere(DbClient db, Function<DbRow,T> transform, String selectStatement, Object... filter) throws ExecutionException, InterruptedException {
    Objects.requireNonNull(db);
    Objects.requireNonNull(transform);
    Objects.requireNonNull(selectStatement);
    Objects.requireNonNull(filter);
    return db.execute(exec -> exec.createNamedQuery(selectStatement).params(filter).execute()).map(row-> row.as(transform)).collectList().get();
  }

}
