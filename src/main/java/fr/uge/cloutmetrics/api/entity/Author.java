package fr.uge.cloutmetrics.api.entity;

import fr.uge.cloutmetrics.api.database.RequestManager;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public record Author(long repositoryId, long userId, long linesContributed) implements DatabaseStorable<Author>  {

  public static void addAuthor(DbClient db, Author author)  throws ExecutionException, InterruptedException {
    RequestManager.insertItem(db, author, "insert-author");
  }

  public static List<Author> getRepositoryAuthors(DbClient db, long repositoryId)  throws ExecutionException, InterruptedException {
    return RequestManager.getItemsWhere(db, Author::create,"select-authors-by-repository-id", repositoryId);
  }

  public static List<Author> getUserAuthors(DbClient db, long userId)  throws ExecutionException, InterruptedException {
    return RequestManager.getItemsWhere(db, Author::create,"select-authors-by-user-id", userId);
  }

  public static Author create(DbRow row){
    Objects.requireNonNull(row);
    return new Author(row.column("REPOSITORYID").as(Long.class),row.column("USERID").as(Long.class),row.column("LINESCONTRIBUTED").as(Long.class));
  }

  @Override
  public Map<String, ?> toNamedParameters() {
    var map = new HashMap<String, Object>(3);
    map.put("repositoryId", repositoryId);
    map.put("userId", userId);
    map.put("linesContributed", linesContributed);
    return map;
  }
}
