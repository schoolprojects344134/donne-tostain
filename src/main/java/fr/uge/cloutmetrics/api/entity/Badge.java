package fr.uge.cloutmetrics.api.entity;

public enum Badge {
  BUILDER, DEVOPS, DOCTOR, ARCHITECT, VILLAGER, WEREWOLF
}
