package fr.uge.cloutmetrics.api.entity;

import fr.uge.cloutmetrics.api.database.RequestManager;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public record BadgeMetric(long repositoryId, long userId, String name) implements DatabaseStorable<BadgeMetric> {

  public static void addBadge(DbClient db, BadgeMetric badge)  throws ExecutionException, InterruptedException {
    RequestManager.insertItem(db, badge, "insert-badge");
  }

  public static List<BadgeMetric> getRepositoryBadges(DbClient db, long repositoryId)  throws ExecutionException, InterruptedException {
    return RequestManager.getItemsWhere(db, BadgeMetric::create,"select-badges-by-repository-id", repositoryId);
  }

  public static List<BadgeMetric> getUserBadges(DbClient db, long userId)  throws ExecutionException, InterruptedException {
    return RequestManager.getItemsWhere(db, BadgeMetric::create,"select-badges-by-user-id", userId);
  }

  public static BadgeMetric create(DbRow row){
    Objects.requireNonNull(row);
    return new BadgeMetric(row.column("REPOSITORYID").as(Long.class), row.column("USERID").as(Long.class), row.column("NAME").as(String.class));
  }

  @Override
  public Map<String, ?> toNamedParameters() {
    var map = new HashMap<String, Object>(3);
    map.put("repositoryId", repositoryId);
    map.put("userId", userId);
    map.put("name", name);
    return map;
  }
}
