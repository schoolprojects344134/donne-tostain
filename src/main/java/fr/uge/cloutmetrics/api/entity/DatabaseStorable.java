package fr.uge.cloutmetrics.api.entity;

import io.helidon.dbclient.DbRow;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public interface DatabaseStorable<T> {
  Map<String,?> toNamedParameters ();
}
