package fr.uge.cloutmetrics.api.entity;

import fr.uge.cloutmetrics.api.database.RequestManager;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public record DocumentationMetric(long repositoryId, long userId, long linesCount) implements DatabaseStorable<DocumentationMetric>{

  public static void addDocumentation(DbClient db, DocumentationMetric documentation)  throws ExecutionException, InterruptedException{
    RequestManager.insertItem(db, documentation,"insert-documentation");
  }

  public static List<DocumentationMetric> getDocumentationByRepositoryId(DbClient db, long repositoryId)  throws ExecutionException, InterruptedException{
    return RequestManager.getItemsWhere(db, DocumentationMetric::create,"select-repository-documentation", repositoryId);
  }

  public static DocumentationMetric create(DbRow row){
    Objects.requireNonNull(row);
    return new DocumentationMetric(row.column("REPOSITORYID").as(Long.class),row.column("USERID").as(Long.class),
            row.column("LINESCOUNT").as(Long.class));
  }

  @Override
  public Map<String, ?> toNamedParameters() {
    var map = new HashMap<String, Object>(3);
    map.put("repositoryId", repositoryId);
    map.put("userId", userId);
    map.put("linesCount", linesCount);
    return map;
  }
}
