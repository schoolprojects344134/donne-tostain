package fr.uge.cloutmetrics.api.entity;

import java.util.Objects;

public enum Language {
  JAVA, JAVASCRIPT, CSHARP, CPP , C, TYPESCRIPT, PYTHON, PHP, HTML, CSS, MARKDOWN, VUE, OTHER;

  public static Language getFromExtension(String extension){
    Objects.requireNonNull(extension);
    return switch (extension) {
      case "java" -> JAVA;
      case "js" -> JAVASCRIPT;
      case "cs" -> CSHARP;
      case "cpp" -> CPP;
      case "c" -> C;
      case "ts" -> TYPESCRIPT;
      case "py" -> PYTHON;
      case "php" -> PHP;
      case "html" -> HTML;
      case "css" -> CSS;
      case "md" -> MARKDOWN;
      case "vue" -> VUE;
      default -> OTHER;
    };
  }
}
