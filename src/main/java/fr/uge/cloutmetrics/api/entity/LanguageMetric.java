package fr.uge.cloutmetrics.api.entity;

import fr.uge.cloutmetrics.api.database.RequestManager;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public record LanguageMetric(long repositoryId, String name, long filesCount) implements DatabaseStorable<LanguageMetric>{

  public static void addLanguage(DbClient db, LanguageMetric language)  throws ExecutionException, InterruptedException{
    RequestManager.insertItem(db, language,"insert-language");
  }

  public static List<LanguageMetric> getLanguagesByRepositoryId(DbClient db, long repositoryId)  throws ExecutionException, InterruptedException{
    return RequestManager.getItemsWhere(db, LanguageMetric::create,"select-repository-languages", repositoryId);
  }

  public static LanguageMetric create(DbRow row){
    Objects.requireNonNull(row);
    return new LanguageMetric(row.column("REPOSITORYID").as(Long.class),row.column("NAME").as(String.class), row.column("FILESCOUNT").as(Long.class));
  }

  @Override
  public Map<String, ?> toNamedParameters() {
    var map = new HashMap<String, Object>(3);
    map.put("repositoryId", repositoryId);
    map.put("name", name);
    map.put("filesCount", filesCount);
    return map;
  }
}
