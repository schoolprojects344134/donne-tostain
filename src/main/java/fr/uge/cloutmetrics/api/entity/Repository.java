package fr.uge.cloutmetrics.api.entity;

import fr.uge.cloutmetrics.api.database.RequestManager;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public record Repository(long repositoryId, String url, String name, String creation_date, String refresh_date, String tokenName, String token, String licence) implements DatabaseStorable<Repository>{

  public static void updateRepository(DbClient db, Repository repository) throws ExecutionException, InterruptedException{
    RequestManager.updateItem(db,repository,"update-repository");
  }

  public static Repository addRepository(DbClient db, Repository repository)  throws ExecutionException, InterruptedException{
    return RequestManager.insertAndGetItem(db, Repository::create, repository,"insert-repository","select-repository");
  }

  public static List<Repository> getAllRepositories(DbClient db) throws ExecutionException, InterruptedException{
    return RequestManager.getAllItems(db, Repository::create, "select-all-repositories");
  }

  public static Optional<Repository>  getRepositoryById(DbClient db, long id)  throws ExecutionException, InterruptedException{
    return RequestManager.getItemBy(db, Repository::create,"select-repository-by-id",id);
  }

  public static Optional<Repository> getRepositoryByUrl(DbClient db, String url)  throws ExecutionException, InterruptedException{
    return RequestManager.getItemBy(db, Repository::create,"select-repository-by-url", url);
  }

  public static Repository create(DbRow row){
    Objects.requireNonNull(row);
    var id = row.column("REPOSITORYID");
    var name = row.column("NAME");
    var url = row.column("URL");
    return new Repository(id.as(Long.class), url.as(String.class),  name.as(String.class),
            row.column("CREATION_DATE").as(String.class), row.column("REFRESH_DATE").as(String.class)
            , row.column("TOKENNAME").as(String.class), row.column("TOKEN").as(String.class), row.column("LICENCE").as(String.class));
  }

  @Override
  public Map<String, ?> toNamedParameters() {
    var map = new HashMap<String, Object>(8);
    map.put("repositoryId", repositoryId);
    map.put("url", url);
    map.put("name", name);
    map.put("creation_date", creation_date);
    map.put("refresh_date", refresh_date);
    map.put("tokenName", tokenName);
    map.put("token", token);
    map.put("licence", licence);
    return map;
  }

}