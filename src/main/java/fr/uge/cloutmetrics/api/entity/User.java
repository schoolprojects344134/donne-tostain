package fr.uge.cloutmetrics.api.entity;

import fr.uge.cloutmetrics.api.database.RequestManager;
import io.helidon.dbclient.DbClient;
import io.helidon.dbclient.DbRow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

public record User(long userId, String pseudo, String email) implements DatabaseStorable<User> {

  public static User addUser(DbClient db, User user)  throws ExecutionException, InterruptedException {
    return getUserByEmail(db, user.email()).orElse(
            RequestManager.insertAndGetItem(db,
                    User::create, user,"insert-user","select-user"
            )
    );
  }

  public static Optional<User> getUserByEmail(DbClient db, String email)  throws ExecutionException, InterruptedException {
    return RequestManager.getItemBy(db, User::create,"select-user-by-email", email);
  }

  public static Optional<User> getUserById(DbClient db, long userId)  throws ExecutionException, InterruptedException {
    return RequestManager.getItemBy(db, User::create,"select-user-by-id", userId);
  }

  public static List<User> getAllUsers(DbClient db) throws ExecutionException, InterruptedException {
    return RequestManager.getAllItems(db, User::create, "select-all-users");
  }

  public static User create(DbRow row){
    Objects.requireNonNull(row);
    var id = row.column("USERID");
    var pseudo = row.column("PSEUDO");
    var email = row.column("EMAIL");
    return new User(id.as(Long.class),pseudo.as(String.class), email.as(String.class));
  }

  @Override
  public Map<String, ?> toNamedParameters() {
    var map = new HashMap<String, Object>(3);
    map.put("userId", userId);
    map.put("pseudo", pseudo);
    map.put("email", email);
    return map;
  }
}
