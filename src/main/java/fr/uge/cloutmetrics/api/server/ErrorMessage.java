package fr.uge.cloutmetrics.api.server;

public record ErrorMessage(String message) {
}
