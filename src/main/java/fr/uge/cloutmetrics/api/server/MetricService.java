package fr.uge.cloutmetrics.api.server;

import fr.uge.cloutmetrics.api.entity.Author;
import fr.uge.cloutmetrics.api.entity.DocumentationMetric;
import fr.uge.cloutmetrics.api.entity.LanguageMetric;
import fr.uge.cloutmetrics.api.jsonparser.JsonParser;
import io.helidon.dbclient.DbClient;
import io.helidon.webserver.Routing;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class MetricService implements Service {
  private final DbClient db;

  public MetricService(DbClient db) {
    Objects.requireNonNull(db);
    this.db = db;
  }

  @Override
  public void update(Routing.Rules rules) {
    rules.get("/metric/languages/{repositoryId}", this::getRepositoryLanguages)
            .get("/metric/authors/{repositoryId}", this::getRepositoryAuthors)
            .get("/metric/documentation/{repositoryId}", this::getRepositoryDocumentation);
  }

  private void getRepositoryLanguages(ServerRequest request, ServerResponse response) {
    try {
      var idRepo = Integer.parseInt(request.path().param("repositoryId"));
      var languages = LanguageMetric.getLanguagesByRepositoryId(db, idRepo);
      response.send(JsonParser.objectToJsonString(languages));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to retrieve repository languages.")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid repository id.")));
    }
  }

  private void getRepositoryDocumentation(ServerRequest request, ServerResponse response) {
    try {
      var idRepo = Integer.parseInt(request.path().param("repositoryId"));
      var documentation = DocumentationMetric.getDocumentationByRepositoryId(db, idRepo);
      response.send(JsonParser.objectToJsonString(documentation));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to retrieve repository documentation.")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid repository id.")));
    }
  }

  private void getRepositoryAuthors(ServerRequest request, ServerResponse response) {
    try {
      var idRepo = Integer.parseInt(request.path().param("repositoryId"));
      var languages = Author.getRepositoryAuthors(db, idRepo);
      response.send(JsonParser.objectToJsonString(languages));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to retrieve repository authors.")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid repository id.")));
    }
  }

}

