package fr.uge.cloutmetrics.api.server;

import fr.uge.cloutmetrics.api.JGit;
import fr.uge.cloutmetrics.api.MetricGeneration;
import fr.uge.cloutmetrics.api.entity.*;
import fr.uge.cloutmetrics.api.jsonparser.JsonParser;
import io.helidon.dbclient.DbClient;
import io.helidon.webserver.Service;
import io.helidon.webserver.Routing;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Handler;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class RepositoryService implements Service {
  private final DbClient db;

  /**
   * Create repository service
   *
   * @param db database client
   */
  public RepositoryService(DbClient db) {
    Objects.requireNonNull(db);
    this.db = db;
  }

  /**
   * A service registers itself by updating the routing rules.
   *
   * @param rules the routing rules.
   */
  @Override
  public void update(Routing.Rules rules) {
    rules.get("/repository/getAll", this::getAllRepository)
            .get("/repository/generateMetrics/{id}", this::generateRepositoryMetrics)
            .get("/repository/{id}", this::getRepositoryById)
            .get("/repository/badges/{id}", this::getRepositoryBadges)
            .post("/repository/add", Handler.create(String.class, this::insertRepositoryHandler));
  }

  private void getRepositoryById(ServerRequest request, ServerResponse response) {
    try {
      var idRepo = Integer.parseInt(request.path().param("id"));
      var repository = Repository.getRepositoryById(db, idRepo).orElseThrow();
      response.send(JsonParser.objectToJsonString(repository));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to get repository.")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid repository id.")));
    }
  }

  private void getRepositoryBadges(ServerRequest request, ServerResponse response) {
    try {
      var idRepo = Integer.parseInt(request.path().param("id"));
      var repository = BadgeMetric.getRepositoryBadges(db, idRepo);
      response.send(JsonParser.objectToJsonString(repository));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to get repositories")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid repository id.")));
    }
  }

  private void getAllRepository(ServerRequest request, ServerResponse response) {
    try {
      var repositories = Repository.getAllRepositories(db);
      response.send(JsonParser.objectToJsonString(repositories));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to get repositories")));
    }
  }

  private void generateRepositoryMetrics(ServerRequest request, ServerResponse response) {
    int idRepo;
    try {
      idRepo = Integer.parseInt(request.path().param("id"));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid repository id.")));
      return;
    }

    Repository repository;
    try {
      var repo = Repository.getRepositoryById(db, idRepo);
      if (repo.isEmpty()) {
        throw new IllegalStateException();
      }
      repository = repo.get();
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
      return;
    } catch (ExecutionException e){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to get repositories.")));
      return;
    }

    var jgit = new JGit(repository);
    MetricGeneration metric;
    try {
      metric = jgit.generateAllData(idRepo);
    } catch (IOException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
      return;
    } catch (GitAPIException g){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to generate metrics from the repository.")));
      return;
    }

    try {
      metric.addInDataBase(db, repository);
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
      return;
    } catch (ExecutionException e){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to compute metrics.")));
      return;
    }
    response.send();
  }

  private void insertRepositoryHandler(ServerRequest request, ServerResponse response, String jsonParam) {
    System.out.println(jsonParam);
    System.out.println("In repo handler");
    var repositoryTemplate = JsonParser.stringToObject(Repository.class, jsonParam);
    var url = repositoryTemplate.url();

    try {
      if (Repository.getRepositoryByUrl(db, url).isPresent()) {
        response.send("Repository " + url + " already existing");
        return;
      }
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
      return;
    } catch (ExecutionException e){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to add the repository.")));
      return;
    }

    var jgit = new JGit(repositoryTemplate);
    Repository repository;
    try {
      repository = jgit.cloneRepo();
    } catch (IOException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
      return;
    } catch (GitAPIException g){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to clone the repository.")));
      return;
    }

    try {
      var newRepository = Repository.addRepository(db, repository);
      response.send(JsonParser.objectToJsonString(newRepository));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e){
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to add the repository.")));
    }
  }
}

