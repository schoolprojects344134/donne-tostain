package fr.uge.cloutmetrics.api.server;

import io.helidon.webserver.WebServer;

import java.nio.file.Path;
import java.util.concurrent.ExecutionException;

public class Server {
  private WebServer server;
  private boolean running = false;

  /**
   * Start Web Server
   */
  public void startWebServer() {
    if (running) {
      throw new IllegalStateException("Server already running");
    }
    running = true;
    var config = ServerConfiguration.createConfig(Path.of("config/dev-config-application.yaml"));
    server = WebServer.builder(ServerConfiguration.createServerConfiguration(config))
            .config(config.get("server"))
            .build();
    server.start().thenAccept(ws -> {
      System.out.println("CloutMetric server started on http://localhost:" + ws.port() + "/");
      ws.whenShutdown().thenRun(() -> System.out.println("CloutMetric server is down..."));
    }).exceptionallyAccept(t -> {
      System.err.println("Startup failed: " + t.getMessage());
      running = false;
    });
  }

  /**
   * Stop web server
   */
  public void shutdownWebServer() {
    if (!running) {
      throw new IllegalStateException("Server is already shutdown.");
    }
    try {
      server.shutdown().get();
    } catch (ExecutionException | InterruptedException e) {
      throw new AssertionError("An error occur while server shutdown.");
    }
    running = false;
  }

}
