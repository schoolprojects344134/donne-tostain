package fr.uge.cloutmetrics.api.server;

import fr.uge.cloutmetrics.api.database.DbManager;
import io.helidon.common.http.Http;
import io.helidon.common.http.MediaType;
import io.helidon.config.Config;
import io.helidon.dbclient.DbClient;
import io.helidon.webserver.Routing;
import io.helidon.webserver.staticcontent.StaticContentSupport;
import io.helidon.openapi.OpenAPISupport;

import java.nio.file.Path;
import java.util.Objects;

import static io.helidon.config.ConfigSources.classpath;

public class ServerConfiguration {
  private ServerConfiguration(){
    throw new UnsupportedOperationException("Instances of ServerConfiguration class are not allowed");
  }

  /**
   * Create the server configuration from a main file configuration
   * @param mainConfigClassPath main server configuration
   * @return server config
   */
  public static Config createConfig(Path mainConfigClassPath){
    Objects.requireNonNull(mainConfigClassPath);
  return Config.builder(classpath("database/repository-statement.yaml")
            ,classpath("database/author-statement.yaml")
            ,classpath("database/dependency-statement.yaml")
            ,classpath("database/language-metrics-statement.yaml")
            ,classpath("database/metrics-statement.yaml")
            ,classpath("database/badge-statement.yaml")
            ,classpath("database/user-statement.yaml")
            ,classpath("database/documentation-statement.yaml")
            ,classpath(mainConfigClassPath.toString())).build();
  }

  /**
   * Create database client
   * @param dbConfig database configuration
   * @return dbclient
   */
  public static DbClient createDatabase(Config dbConfig){
    Objects.requireNonNull(dbConfig);
    var dbClient = DbClient.builder(dbConfig).build();
    DbManager.init(dbClient);
    return dbClient;
  }

  private static void registerRepositoryService(Routing.Builder builder, DbClient dbClient){
    builder.register(new RepositoryService(dbClient));
  }

  private static void registerMetricService(Routing.Builder builder, DbClient dbClient){
    builder.register(new MetricService(dbClient));
  }

  private static void registerUserService(Routing.Builder builder, DbClient dbClient){
    builder.register(new UserService(dbClient));
  }

  private static Routing.Builder registerWebRoute(Routing.Builder builder){
     return builder.register("/", StaticContentSupport.builder("Web")
        .contentType("html", MediaType.TEXT_HTML)
        .build())
        .register("/js", StaticContentSupport.builder("Web/js")
            .contentType("js", MediaType.APPLICATION_JAVASCRIPT)
            .build())
        .register("/css", StaticContentSupport.builder("Web/css")
            .contentType("css", MediaType.WILDCARD)
            .build());
  }

  private static Routing.Builder registerOpenApi(Routing.Builder builder){
    return builder.register(OpenAPISupport.create());
  }

  private static Routing.Builder registerUnhandledRoute(Routing.Builder builder){
    return builder.any("/",(req, res) -> {
      res.status(Http.Status.MOVED_PERMANENTLY_301);
      res.headers().put(Http.Header.LOCATION, "/home.html");
      res.send();
    }).any((req, res) -> {
      res.status(Http.Status.NOT_FOUND_404);
      //TODO add page error 404
      res.send();
    });
  }

  /**
   * Create server configuration with services and routes
   *
   * @param config server configuration
   * @return routing configured
   */
  public static Routing createServerConfiguration(Config config) {
    Objects.requireNonNull(config);
    var db = createDatabase(config.get("db"));
    var builder = Routing.builder();
    registerWebRoute(builder);
    registerMetricService(builder,db);
    registerRepositoryService(builder,db);
    registerUserService(builder,db);
    registerUnhandledRoute(builder);
    return builder.build();
  }


}
