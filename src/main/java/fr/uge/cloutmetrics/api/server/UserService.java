package fr.uge.cloutmetrics.api.server;

import fr.uge.cloutmetrics.api.entity.User;
import fr.uge.cloutmetrics.api.entity.Author;
import fr.uge.cloutmetrics.api.entity.BadgeMetric;
import fr.uge.cloutmetrics.api.jsonparser.JsonParser;
import io.helidon.dbclient.DbClient;
import io.helidon.webserver.Routing;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class UserService implements Service {
  private final DbClient db;

  /**
   * Constructor for User service
   *
   * @param db db client to used
   */
  public UserService(DbClient db) {
    Objects.requireNonNull(db);
    this.db = db;
  }

  @Override
  public void update(Routing.Rules rules) {
    Objects.requireNonNull(rules);
    rules.get("/user/getAll", this::getAllUser)
            .get("/user/{userId}", this::getUserById)
            .get("/user/authors/{userId}", this::getUserAuthors)
            .get("/user/badges/{id}", this::getUserBadges);
  }

  private void getAllUser(ServerRequest request, ServerResponse response) {
    try {
      var users = User.getAllUsers(db);
      response.send(JsonParser.objectToJsonString(users));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to retrieve users.")));
    }
  }

  private void getUserById(ServerRequest request, ServerResponse response) {
    try {
      var userId = Integer.parseInt(request.path().param("userId"));
      var user = User.getUserById(db, userId).orElseThrow();
      response.send(JsonParser.objectToJsonString(user));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to retrieve the user.")));
    } catch (NoSuchElementException n) {
      response.status(404);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, no user found.")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid user id.")));
    }
  }

  private void getUserAuthors(ServerRequest request, ServerResponse response) {
    try {
      var userId = Integer.parseInt(request.path().param("userId"));
      var authorsForUser = Author.getUserAuthors(db, userId);
      response.send(JsonParser.objectToJsonString(authorsForUser));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to retrieve user contributions.")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid user id.")));
    }
  }

  private void getUserBadges(ServerRequest request, ServerResponse response) {
    try {
      var idRepo = Integer.parseInt(request.path().param("id"));
      var repository = BadgeMetric.getUserBadges(db, idRepo);
      response.send(JsonParser.objectToJsonString(repository));
    } catch (InterruptedException i) {
      response.status(500);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Server internal error.")));
    } catch (ExecutionException e) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error with request execution, failed to retrieve user badges.")));
    } catch (NumberFormatException nu) {
      response.status(400);
      response.send(JsonParser.objectToJsonString(new ErrorMessage("Error, invalid repository id.")));
    }
  }
}

