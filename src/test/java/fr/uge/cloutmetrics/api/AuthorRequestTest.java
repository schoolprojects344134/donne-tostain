package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.database.DbManager;
import fr.uge.cloutmetrics.api.entity.Author;
import fr.uge.cloutmetrics.api.server.ServerConfiguration;
import io.helidon.config.Config;
import io.helidon.dbclient.DbClient;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.concurrent.ExecutionException;

import static io.helidon.config.ConfigSources.classpath;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Nested
public class AuthorRequestTest {
  private static DbClient createTestDbClient(){
    var config = ServerConfiguration.createConfig(Path.of("config/test-config-application.yaml"));
    var db = ServerConfiguration.createDatabase(config.get("db"));
    DbManager.clear(db);
    DbManager.init(db);
    return db;
  }

    @Test
    public void insertAuthor() throws InterruptedException, ExecutionException {
      var db = createTestDbClient();
      var author = new Author(1,  1,2000);
      Author.addAuthor(db,author);
      var authorList = Author.getRepositoryAuthors(db,1);
      assertEquals("[Author[repositoryId=1, userId=1, linesContributed=2000]]",authorList.toString());
    }

  @Test
  public void getRepositoryAuthors() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var author = new Author(1,  1,2000);
    var author2 = new Author(1,  2,1200);
    var author3 = new Author(2,  1,11);
    Author.addAuthor(db,author);
    Author.addAuthor(db,author2);
    Author.addAuthor(db,author3);
    var authorList = Author.getRepositoryAuthors(db,1);
    assertEquals("[Author[repositoryId=1, userId=1, linesContributed=2000], Author[repositoryId=1, userId=2, linesContributed=1200]]",authorList.toString());
  }

  @Test
  public void getUserAuthors() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var author = new Author(1,  1,2000);
    var author2 = new Author(1,  2,1200);
    var author3 = new Author(2,  1,11);
    Author.addAuthor(db,author);
    Author.addAuthor(db,author2);
    Author.addAuthor(db,author3);
    var userAuthoringList = Author.getUserAuthors(db,1);
    assertEquals("[Author[repositoryId=1, userId=1, linesContributed=2000], Author[repositoryId=2, userId=1, linesContributed=11]]",userAuthoringList.toString());
  }

}
