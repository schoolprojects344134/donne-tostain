package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.database.DbManager;
import fr.uge.cloutmetrics.api.entity.Author;
import fr.uge.cloutmetrics.api.entity.Badge;
import fr.uge.cloutmetrics.api.entity.BadgeMetric;
import fr.uge.cloutmetrics.api.server.ServerConfiguration;
import io.helidon.config.Config;
import io.helidon.dbclient.DbClient;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.concurrent.ExecutionException;

import static io.helidon.config.ConfigSources.classpath;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Nested
public class BadgeRequestTest {
  private static DbClient createTestDbClient(){
    var config = ServerConfiguration.createConfig(Path.of("config/test-config-application.yaml"));
    var db = ServerConfiguration.createDatabase(config.get("db"));
    DbManager.clear(db);
    DbManager.init(db);
    return db;
  }

    @Test
    public void insertBadge() throws InterruptedException, ExecutionException {
      var db = createTestDbClient();
      var badge = new BadgeMetric(1, 1, Badge.DOCTOR.name());
      BadgeMetric.addBadge(db,badge);
      var badgesList = BadgeMetric.getRepositoryBadges(db,1);
      assertEquals("[BadgeMetric[repositoryId=1, userId=1, name=DOCTOR]]",badgesList.toString());
    }

  @Test
  public void getRepositoryBadges() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var badge = new BadgeMetric(1, 1, Badge.DOCTOR.name());
    var badge2 = new BadgeMetric(1, 1, Badge.DEVOPS.name());
    var badge3 = new BadgeMetric(2, 1, Badge.DOCTOR.name());
    BadgeMetric.addBadge(db,badge);
    BadgeMetric.addBadge(db,badge2);
    BadgeMetric.addBadge(db,badge3);
    var badgesList = BadgeMetric.getRepositoryBadges(db,1);
    assertEquals("[BadgeMetric[repositoryId=1, userId=1, name=DOCTOR], BadgeMetric[repositoryId=1, userId=1, name=DEVOPS]]",badgesList.toString());
  }

  @Test
  public void getUserBadges() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var badge = new BadgeMetric(1, 1, Badge.DOCTOR.name());
    var badge2 = new BadgeMetric(1, 1, Badge.DEVOPS.name());
    var badge3 = new BadgeMetric(2, 1, Badge.DOCTOR.name());
    var badge4 = new BadgeMetric(2, 2, Badge.DEVOPS.name());
    BadgeMetric.addBadge(db,badge);
    BadgeMetric.addBadge(db,badge2);
    BadgeMetric.addBadge(db,badge3);
    BadgeMetric.addBadge(db,badge4);
    var badgesList = BadgeMetric.getUserBadges(db,1);
    assertEquals("[BadgeMetric[repositoryId=1, userId=1, name=DOCTOR], BadgeMetric[repositoryId=1, userId=1, name=DEVOPS], BadgeMetric[repositoryId=2, userId=1, name=DOCTOR]]",badgesList.toString());
  }

}
