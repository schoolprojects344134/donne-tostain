package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.database.DbManager;
import fr.uge.cloutmetrics.api.entity.LanguageMetric;
import fr.uge.cloutmetrics.api.server.ServerConfiguration;
import io.helidon.config.Config;
import io.helidon.dbclient.DbClient;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.concurrent.ExecutionException;

import static io.helidon.config.ConfigSources.classpath;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Nested
public class LanguageMetricRequestTest {
  private static DbClient createTestDbClient(){
    var config = ServerConfiguration.createConfig(Path.of("config/test-config-application.yaml"));
    var db = ServerConfiguration.createDatabase(config.get("db"));
    DbManager.clear(db);
    DbManager.init(db);
    return db;
  }

    @Test
    public void insertLanguage() throws InterruptedException, ExecutionException {
      var db = createTestDbClient();
      var metric = new LanguageMetric(1,  "java",4000000);
      LanguageMetric.addLanguage(db,metric);
      var languageList = LanguageMetric.getLanguagesByRepositoryId(db,1);
      assertEquals("[LanguageMetric[repositoryId=1, name=java, filesCount=4000000]]",languageList.toString());
    }

  @Test
  public void getLanguagesByRepositoryId() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var metric = new LanguageMetric(1,  "java",4000000);
    var metric2 = new LanguageMetric(1,  "js",5000);
    var metric3 = new LanguageMetric(2,  "java",5);
    LanguageMetric.addLanguage(db,metric);
    LanguageMetric.addLanguage(db,metric2);
    LanguageMetric.addLanguage(db,metric3);
    var languageList = LanguageMetric.getLanguagesByRepositoryId(db,1);
    assertEquals("[LanguageMetric[repositoryId=1, name=java, filesCount=4000000], LanguageMetric[repositoryId=1, name=js, filesCount=5000]]",languageList.toString());
  }




}
