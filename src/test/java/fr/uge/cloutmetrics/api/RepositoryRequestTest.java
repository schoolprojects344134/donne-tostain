package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.database.DbManager;
import fr.uge.cloutmetrics.api.entity.Repository;
import fr.uge.cloutmetrics.api.server.ServerConfiguration;
import io.helidon.config.Config;
import io.helidon.dbclient.DbClient;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static io.helidon.config.ConfigSources.classpath;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Path;
import java.util.concurrent.ExecutionException;

@Nested
public class RepositoryRequestTest {
  private static DbClient createTestDbClient(){
    var config = ServerConfiguration.createConfig(Path.of("config/test-config-application.yaml"));
    var db = ServerConfiguration.createDatabase(config.get("db"));
    DbManager.clear(db);
    DbManager.init(db);
    return db;
  }

    @Test
    public void insertRepository() throws InterruptedException, ExecutionException {
      var db = createTestDbClient();
      var repository = new Repository(-1,"CUnTest.com","GoodTest","2022-10-30","2022-10-30", "","", "");
      Repository result = Repository.addRepository(db,repository);
      assertEquals("Repository[repositoryId=1, url=CUnTest.com, name=GoodTest, creation_date=2022-10-30, refresh_date=2022-10-30, tokenName=, token=, licence=]",result.toString());
    }

  @Test
  public void getRepositoryById() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var generateRepository = new Repository(-1,"CUnTest.com","GoodTest","2022-10-30","2022-10-30","","", "");
    Repository result = Repository.addRepository(db,generateRepository);
    var repository = Repository.getRepositoryById(db,result.repositoryId()).get();
    assertEquals("Repository[repositoryId=1, url=CUnTest.com, name=GoodTest, creation_date=2022-10-30, refresh_date=2022-10-30, tokenName=, token=, licence=]",repository.toString());
  }

  @Test
  public void updateRepository() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var generateRepository = new Repository(-1,"CUnTest.com","GoodTest","2022-10-30","2022-10-30","","", "");
    var result = Repository.addRepository(db,generateRepository);
    var newRepositoryInformation = new Repository(result.repositoryId(),"newUrlTest.com", "veryGoodTest","2023-01-01","2023-02-02","","", "");
    Repository.updateRepository(db, newRepositoryInformation);
    var repository = Repository.getRepositoryById(db,result.repositoryId()).get();
    assertEquals("Repository[repositoryId=1, url=newUrlTest.com, name=veryGoodTest, creation_date=2023-01-01, refresh_date=2023-02-02, tokenName=, token=, licence=]",repository.toString());
  }

  @Test
  public void getAllRepository() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var repo1 = new Repository(-1,"CUnTest.com","GoodTest","2022-10-30","2022-10-30","","", "");
    var repo2 = new Repository(-1,"CUnSecondTest.com","WowTest","2022-10-30","2022-10-30","","", "");
    Repository.addRepository(db,repo1);
    Repository.addRepository(db,repo2);
    var repoList = Repository.getAllRepositories(db);
    assertEquals("[Repository[repositoryId=1, url=CUnTest.com, name=GoodTest, creation_date=2022-10-30, refresh_date=2022-10-30, tokenName=, token=, licence=]," +
            " Repository[repositoryId=2, url=CUnSecondTest.com, name=WowTest, creation_date=2022-10-30, refresh_date=2022-10-30, tokenName=, token=, licence=]]",repoList.toString());
  }

  @Test
  public void getRepositoryByUrl() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var repo1 = new Repository(-1,"CUnTest.com","GoodTest","2022-10-30","2022-10-30","","", "");
    Repository.addRepository(db,repo1);
    var repository = Repository.getRepositoryByUrl(db,repo1.url()).get();
    assertEquals("Repository[repositoryId=1, url=CUnTest.com, name=GoodTest, creation_date=2022-10-30, refresh_date=2022-10-30, tokenName=, token=, licence=]",repository.toString());
  }

}
