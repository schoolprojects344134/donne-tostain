package fr.uge.cloutmetrics.api;

import fr.uge.cloutmetrics.api.database.DbManager;
import fr.uge.cloutmetrics.api.entity.User;
import fr.uge.cloutmetrics.api.server.ServerConfiguration;
import io.helidon.config.Config;
import io.helidon.dbclient.DbClient;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;

import static io.helidon.config.ConfigSources.classpath;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Nested
public class UserRequestTest {
  private static DbClient createTestDbClient(){
    var config = ServerConfiguration.createConfig(Path.of("config/test-config-application.yaml"));
    var db = ServerConfiguration.createDatabase(config.get("db"));
    DbManager.clear(db);
    DbManager.init(db);
    return db;
  }

    @Test
    public void insertUser() throws InterruptedException, ExecutionException {
      var db = createTestDbClient();
      var user = new User(-1,  "toto","toto@uge.com");
      var result = User.addUser(db,user);
      assertEquals("User[userId=1, pseudo=toto, email=toto@uge.com]",result.toString());
    }

  @Test
  public void getAllUsers() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var user1 = new User(-1,  "toto","toto@uge.com");
    var user2 = new User(-1,  "tata","tata@uge.com");
    User.addUser(db,user1);
    User.addUser(db,user2);
    var userList = User.getAllUsers(db);
    assertEquals("[User[userId=1, pseudo=toto, email=toto@uge.com], User[userId=2, pseudo=tata, email=tata@uge.com]]",userList.toString());
  }

  @Test
  public void getUser() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var user1 = new User(-1,  "toto","toto@uge.com");
    User.addUser(db,user1);
    var userResponse = User.getUserByEmail(db,"toto@uge.com").get();
    assertEquals("User[userId=1, pseudo=toto, email=toto@uge.com]",userResponse.toString());
  }

  @Test
  public void getUserNotExist() throws InterruptedException, ExecutionException {
    var db = createTestDbClient();
    var user1 = new User(-1,  "toto","titi@uge.com");
    User.addUser(db,user1);
    assertThrows(NoSuchElementException.class, () -> User.getUserByEmail(db,"toto@uge.com").get());
  }

}
